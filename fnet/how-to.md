# @fnet/expression Developer Guide

## Overview

The `@fnet/expression` library is designed to parse and dissect custom string expressions into their respective components, allowing developers to easily manage structured data expressions. The library's primary functionality allows users to break down expressions into processors and statements, even supporting nested expressions up to a specified depth. This enables structured parsing of expressions for various use cases such as data processing pipelines, configuration management, or templating systems.

## Installation

To use `@fnet/expression` in your project, you can install it via npm or yarn:

### Using npm

```bash
npm install @fnet/expression
```

### Using yarn

```bash
yarn add @fnet/expression
```

## Usage

Here's a practical example of how to use the `@fnet/expression` library to parse a custom expression format and retrieve its components:

```javascript
import parseExpression from '@fnet/expression';

// Example expression string
const myExpression = 'filter::sort::capitalize::Transform this text';

// Parse the expression
const parsedData = parseExpression({ expression: myExpression });

// Display the parsed components
console.log(parsedData);
```

## Examples

Below are some examples demonstrating common use cases of the library:

### Parsing an Expression

```javascript
import parseExpression from '@fnet/expression';

const expression = 'process1::step2::finalize::Process the data fully';
const result = parseExpression({ expression });

console.log(result);

// Expected Output
// {
//   processor: 'process1',
//   statement: 'step2::finalize::Process the data fully',
//   expression: 'process1::step2::finalize::Process the data fully',
//   process: {
//     statement: 'Process the data fully',
//     order: ['process1', 'step2', 'finalize']
//   },
//   next: {
//     processor: 'step2',
//     ...
//   }
// }
```

### Handling Nested Expressions

The library can handle nested expressions and limit recursion depth to maintain performance and avoid infinite loops:

```javascript
import parseExpression from '@fnet/expression';

const nestedExpression = 'outer::inner::deepest::Execute the chain';
const parsedResult = parseExpression({ expression: nestedExpression, depth: 3 });

console.log(parsedResult);

// Expected Output includes structured breakdown of each level,
// showing processors and statements for each nested level
```

## Acknowledgement

The development of `@fnet/expression` was guided by community-driven requirements and contributions. The structure and approach for parsing expressions are inspired by best practices in data parsing and processing.