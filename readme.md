# @fnet/expression

## Introduction
The `@fnet/expression` project is designed to help users parse and analyze structured expressions that consist of processor and statement components. This tool can be particularly useful for those who need to dissect complex expressions into more manageable parts, allowing for easier understanding and processing.

## How It Works
The project works by taking a structured expression and breaking it down into its basic components: processors and statements. It uses a recursive parsing method to handle nested expressions while respecting a maximum depth to prevent too deep recursion. The expression is analyzed to extract all processors, presenting a clear hierarchy and order of the components.

## Key Features
- **Recursive Parsing**: Efficiently breaks down expressions into individual components, even handling nested structures.
- **Processor Extraction**: Gathers all processor elements from the expression, providing an ordered list.
- **Depth Management**: Prevents overly deep recursion by setting a limit, ensuring performance and reliability.

## Conclusion
The `@fnet/expression` project is a straightforward and practical tool for parsing and analyzing structured expressions. It offers users a reliable method to dissect intricate expressions into simpler parts, making them easier to work with and understand.