/**
 * Recursively parses an expression to break it into processor and statement components.
 * The function respects a maximum depth to prevent infinite recursion.
 * 
 * @param {Object} param0 - Contains the expression to be parsed and the current depth level.
 * @param {string} param0.expression - The expression to be parsed.
 * @param {number} [param0.depth=5] - The current depth level.
 * @returns {Object|null} - Returns the parsed expression or null if it doesn't match the pattern.
 */
function parseExpression({ expression, depth = 5 }) {
  // Return null if maximum depth is reached
  if (depth <= 0) return null;

  // Regex pattern to match processor and statement
  const patternBase = /^([a-z][a-z0-9_-]*)::([^\s][\s\S]*)$/;
  let matches;

  // Validate expression against the pattern
  if (patternBase.test(expression)) {
      matches = expression.match(patternBase);
  } else return null;

  const processor = matches[1];
  const statement = matches[2];
  // Recursively parse nested expressions
  const next = parseExpression({ expression: statement, depth: depth - 1 });

  const result = {
      processor,
      statement,
      expression
  };

  // Include nested expression in the result if exists
  if (next) {
      result.next = next;
  }

  return result;
}

/**
* Extracts all processors from the parsed expression.
* 
* @param {Object} parsed - The parsed expression.
* @returns {Array<string>} - Returns an array of processors.
*/
function extractProcessors(parsed) {
  if (!parsed) return [];

  const processors = extractProcessors(parsed.next);
  processors.push(parsed.processor);
  return processors;
}

/**
* The main function to parse an expression and return its components.
* 
* @param {Object} param0 - Contains the expression to be parsed.
* @param {string} param0.expression - The expression to be parsed.
* @returns {Object|null} - Returns the components of the parsed expression or null if it doesn't match the pattern.
*/
export default ({ expression }) => {
  const parsed = parseExpression({ expression });

  if (!parsed) return null;

  // Find the deepest nested expression
  let current = parsed;
  while (current && current.next) {
      current = current.next;
  }

  const result = {
      processor: parsed.processor,
      statement: parsed.statement,
      expression: parsed.expression,
      process: {
          statement: current.statement,
          order: extractProcessors(parsed)
      }
  };

  // Include nested expression if available
  if (parsed.next) {
      result.next = parsed.next;
  }

  return result;
};